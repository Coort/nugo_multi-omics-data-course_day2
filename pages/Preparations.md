# Download PathVisio
Instructions on how to download PathVisio v3.3.0 can be found [here](https://pathvisio.github.io/downloads)

Pleas note that PathVisio requires the installation of Java 8, but not higher! You can download Java [here](https://www.java.com/nl/download/)

-----

# Download Data files.
The datafile including transcriptomics and metabolomics data is available [here](https://gitlab.com/coort/nugo_multi-omics-data-course_day2/raw/master/data/dataset.txt?inline=false)

## Additional information
### Combining data types like transcriptomics,proteomics and metabolomics
We prepared the data for you, but be aware that all data needs to be combined in one data file. Very likely, you are using different identifiers for your different measurements (e.g. Ensemnbl  for transcriptomics, UniProt for proteomics and HMDB for metabolomics). Each identifier needs to be accompanied by the appropriate database/system code. A list with system code can be found [here](https://bridgedb.github.io/pages/system-codes). Additionally, we advise to add a type column, indicating what data type/experiment the measurement comes from. The data file should look like this (with many more measurements per type).

| Identifier | System Code | log2FC | p-value |	Type |
| ---------- | ----------- | ------ | ------- | ---- | 
| ENSG00000019186 |	En | -1.2 |	0.02 |Transcriptomics |
| P12821 |	S |	-0.8 |	0.04 |	Proteomics |
| HMDB11747 | Ch | -1.0 | 0.03 | Metabolomics |

----

## Download the human identifier (ID) mapping gene database. 
You can download the human ID mapping database [here](https://zenodo.org/record/3667670/files/Hs_Derby_Ensembl_91.bridge?download=1) and save it on your computer/laptop.  The "Hs_Derby_Ensembl_91.bridge” file is needed to identify the genes in the pathway and the dataset.

## Download the metabolite mapping database. 
You can download the metabolite mapping database [here](https://ndownloader.figshare.com/files/24180464) and save it on your computer/laptop.  
"metabolites_20200809.bridge". The "metabolites_20200809.bridge" file is needed to identify the metabolites in the pathway and the dataset. 	

## Download the human pathway collection of WikiPathways.
Download the human pathway collection from Wikipathways [here](https://gitlab.com/coort/nugo_multi-omics-data-course_day2/raw/master/data/Human_pathways.zip?inline=false). Create a new folder: Human_pathways. Thereafter unzip the directory and extract the pathways in the newly created Human_pathways folder

-----

## Download Cytoscape

Go the Cytoscape [website](https://github.com/cytoscape/cytoscape/releases/3.7.2/) and download version 3.7.2

### Install Apps in Cytoscape

Open Cytoscape and go to Apps -> App Manager and install the following Cytoscape apps: 
1.	stringApp
2.	WikiPathways
3.	CyTargetLinker 
4.	CyNDEx-2

Restart Cytoscape after installing the Apps. 

### Download Linksets
Download the ChEMBL and WikiPathways linksets [here](https://gitlab.com/coort/nugo_multi-omics-data-course_day2/raw/master/data/LinkSets.zip?inline=false)
Create a new folder: Linksets and unzip directory and extract the two linksets in the newly created folder.




