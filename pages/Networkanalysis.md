# Assignment 1: Protein-protein interaction network of proteins involved in Rickets caused by vitamin D deficiency

## Step 1.1: Import and extend a Vitamin D deficiency Ricket PPI network from STRING in Cytoscape

Rickets is the softening and weakening of bones in children, usually because of an extreme and prolonged vitamin D deficiency.  
First we will use the stringApp to create a protein-protein interaction (PPI) network for the Vitamin D deficiency (Disease Query)

* Select the “Disease query”

![alt text](img/img_string.png "STRING disease query")

* Set the criteria by selecting “more options” to a confidence (score) cut-off = 0.4 and maximal number of proteins = 20

![alt text](img/img_criteria.png "Set criteria")

* Search for the “vitamin D hydroxylation-deficient rickets”.

-----

## The PPI network linked to Vitamin D deficiency is now generated in Cytoscape.

#### Question 1: Investigation of the STRING Vitamin deficienct network
> - How many nodes are in the network and are they all connected? 
> - How many edges are in the network?
> - Which types of interaction evidence are present in the PPI network?
> - Which PPI has the highest score? 
> - Which additional information related to the proteins is given?


## Step 1.2: Extend network with compound information

Now you are going to extend the PPI Vitamin D deficiency network using CyTargetLinker with compound-target interactions from ChEMBL and pathway-gene associations from WikiPathways. ChEMBL is an open online bioactivity database containing information about compounds, their bioactivity and their possible targets (including proteins). WikiPathways is an online curated database for biological pathways. 

* Go to Apps -> CyTargetLinker -> Extend network
    * Select User Network
        * choose the network name of the STRING network
    * Select your network attribute	
        * choose the name of the column containing a biological identifier (“canonical name”)
    * Select Link Sets
        * Browse files for the directory which contains the Link Sets 
    * Select direction 
        * “SOURCES”
* Click at Ok

![alt text](img/Cytargetlinker6.PNG "Set CyTargetlinker criteria")

* Select the ChEMBL (chembl_23_hsa_20180126.xgmml) LinkSet
* Click at OK

![alt text](img/Cytargetlinker4.PNG "Select LinkSets")

* Go to the control panel -> Style
    * Select Label -> column: Canonical name and Mapping Type: Passthrough mapping

![alt text](img/Style.PNG "Style")

####  Open the CyTargetLinker results panel.
* Go to Apps -> CyTargetLinker-> Show Results Panel. 
* At the right hand side the panel is opened and select the Extended network (CTL_String Network-Rickets).
* Now the amount of added interactions per Linkset is shown.


#### Question 2: Investigation of extended STRING Rett network
> - How many nodes are in the network and are they all connected? 
> - How many edges are in the network?
> - How many interactions are added from ChEMBL ? 
> - What type of information does ChEMBL add to the network? 

----

# Assignment 2: Extend the Nucleotide metabolism (WP404) pathway in WikiPathways with other processes

## Step 2.1: In this part, we are going to open a pathway from WikiPathways as a network and extend it with regulatory interactions. We will use two different Cytoscape apps: WikiPathways and CyTargetLinker 

In Cytoscape:
* File → Import → Network from Public Databases.
    * Select WikiPathways as a Data Source
    * Search for "Nuleotide metabolism" and select only human pathways
    * Select the “Nucleotide metabolism (WP404)
* “Import as Pathway”
* Now import again as Network
* Both the pathway and the network view are created


![alt text](img/Nucleotide.PNG "Select pathways from WikiPathways")


#### Question 3: Comparing “Pathway” and “Network”
> - List the main differences between the pathway and the network view of the selected process  

## Step 2.2: Now you are going to extend Nucleotide metabolism (WP404) pathway visualized as a network using CyTargetLinker with gene-pathway interactions from WikiPathways.
* Go to Apps -> CyTargetLinker -> Extend network
    * Select User Network
        *choose the nucleotide metabolism pathway as a network
    * Select your network attribute	
        * choose the name of the column containing a biological identifier (“Shared name”)
    * Select Link Sets
        * Browse in the directory which contains the LinkSets and select WikiPathways: 
    * Select direction 
        * “BOTH”
•	Click at OK

![alt text](img/Select_WP.PNG "Extend with pathways from WikiPathways")

* Select the human WikiPathways analysis collection (wikipathways-20190610-hsa.xgmml) LinkSet
* Click at OK
* Open the results panel via Apps -> CyTargetLinker -> Show Results Panel

![alt text](img/Select_WP2.PNG "Extend with pathways from WikiPathways")

#### Question 4: Extended pathway with other human pathways from WikiPathways
> - How many pathway-gene interactions are added to the network?  
> - Which gene(s) is(are) connected to the highest amount of pathways?






