# Assignment 1: Open pathway in PathVisio

## Step 1.1: Open PathVisio
* On Windows: start PathVisio by double-clicking the pathvisio.bat file
* On MacOSX / Linux: start PathVisio by running pathvisio.sh or double-click the pathvisio.jar file
 
## Step 1.2: Open a pathway in PathVisio
Go to File -> Open and select the "Hs_Nucleotide_Metabolism_WP404_107158.gpml" file from the human WikiPathways collection. 


#### Question 1
> - Find and click on the POLA gene.
> - With which identifier and database is this gene annotated? (Check the “Backpage” tab on the right side).

## Step 1.3: Load the mapping databases
### ID gene mapping database
1. Go to Data → Select Gene Database → Browse to Hs_Derby_Ensembl_91.bridge and select this file. 
2. Go to Data-> Select Metabolite database  Browse to metabolites_20200809.bridge and select this file. 
3. Check the status bar at the bottom to see if the gene database has been loaded.

#### Question 2 
> - Select the POLA gene again and go to the “Backpage” tab.
> - Can you now also find the Ensembl identifier of this gene?


# Assignment 2: Data import in PathVisio

Import the data as described below. Go through the different dialogs and before you click finish, answer the questions at the end!

## Step 2.1: Description of Data Import
In PathVisio go to Data → Import expression data, select the dataset (dataset.txt) as the input file. Everything else should be filled in automatically (see Figure 2a).
In the next dialog, make sure the correct separators are used. You should see the different columns in the preview (see Figure 2b).

![alt text](img/Figure2a.PNG "Figure 2a")

![alt text](img/Figure2b.PNG "Figure 2b")

Important: in this step you need to select the column that contains the identifier and column with the system codes. Select the database you chose in question A (note: if the database is wrong your identifiers will not be recognized correctly), see Figure 2c.

![alt text](img/Figure2c.PNG "Figure 2c")

Now the data gets imported (see Figure 2d ). Before clicking finish answer the questions below:

![alt text](img/Figure2e.png "Figure 2d")

#### Question 3
> - How many rows were successfully imported?

#### Question 4
> - How many identifiers were not recognized? 
> - What does that mean?
> - Important: if the number of rows is the same as the number of identifiers not recognized the data import was not done correctly.

If you click finish, you should see a default visualization on the pathway (if all genes are gray, the data import was not successful). If there is no pathway open, you can check the status bar at the bottom where the dataset will be listed.


# Assignment 3: Creating a basic visualization
Follow the instruction to create a basic visualization:
1. Go to Data → Visualization Options
2. Create a new visualization named “basic visualization”

![alt text](img/Viz1.png "select visualization")

3. Select “Expression as color” and “Text label”.
4. In “Expression as color” select “Basic”.
5. Check the checkbox before “logFC” and define a new color set.

![alt text](img/Viz2.png "select logFC")

6. Select “Gradient” and define a gradient from -1 over 0 to 1 (blue – white – red) → Click OK.

![alt text](img/Gradient.PNG "gradient")

#### Question 5
> - Make a screenshot of the pathway.
> - What do the colors in the pathway mean biologically? ( Hint : Check the “Legend” tab on the right side).

#### Question 6. 
> - Select the POLA gene (top left), go to the “Data” tab.
> - What is the logFC of the POLA gene?


# Assignment 4: Creating an advanced visualization

PathVisio also allows users to visualize multiple data columns together. For that we need to create a new advanced visualization.

1. Select “Expression as color” and “Text label”.
2. In “Expression as color” select “ Advanced ” instead of " Basic".

3. Repeat the visualization for the logFC as performed in Assignment 3.

![alt text](img/Viz3.png "select advanced")

## Creat visualization for pvalue

4. Check the checkbox before “P.Value” and define a new color set, see Figure.
5. At the bottom, click on “Add Rule”. Go to the text field next to “Rule Logic” and specify the following criteria: [P.Value] <= 0.05. Then click on color and select a light green. Add another rule with this criteria: [P.Value] > 0.05. Then click on color and select white. Click OK and OK.

![alt text](img/pvalue.PNG "pvalue")

## Creat visualization for type

6. Check the checkbox before “Type” and define a new color set, see Figure.
7. At the bottom, click on “Add Rule”. Go to the text field next to “Rule Logic” and specify the following criteria: [Type] = "Transcriptomics". Then click on color and select orange. Add another rule with this criteria: [Type]="Metabolomics". Then click on color and select yellow. Click OK and OK.

![alt text](img/Type.PNG "type")

#### Question 7. 
> - Make a screenshot of the pathway.
> - What do the colors in the different columns on the data nodes in the pathway mean biologically? (Hint : Check the “Legend” tab on the right side).

#### Question 8. 
> - How many significant genes (P.value < 0.05) are in the pathway?
> - How many significant metabolites (P.value < 0.05) are in the pathway?



# Assignment 5: Perform pathway statistics
To identify pathways that might be affected after the active form of vitamin D in THP-1 cells, you can perform pathway statistics to calculate Z-Scores for each pathway (check lecture!). PathVisio automatically ranks the pathways based on the Z-Score.
● Go to Data → Statistics
● First we need to define a criteria for differentially expressed genes. We are going to select those genes and metabolites based on significant p-value but we are also going to make sure the change is high enough by specifying a logFC threshold:
([logFC] < -0.26 OR [logFC] > 0.26) AND [P.Value] < 0.05

#### Question 9. Explain in your own words what this expression criteria means (whichgenes will be selected)?
> - ([logFC] < -0.26 OR [logFC] > 0.26) AND [adj.P.Val] < 0.05

● Now we need to specify the pathway directory. In the BBS3021PathwayAnalysis-Data folder you created the directory: Human_pathways
Browse to this directory and select it.
● Then click on Calculate and wait for the result table

#### Question 10. 
> - What are the top 5 altered pathways and what are their Z-Scores?

#### Question 11
> - Are the affected processes known to be influenced by the active form of hormonally active vitamin D? 
> - How are the genes and/or metabolites in the pathways altered?  (investigate three pathways in more detail)

# Assignment 6: Export pathway figures
The pathway diagrams with or without data can be exported as an image.
Open the highest ranked pathway after performing the pathway statistical analysis. Go to File -> Export and export the image as .png. Now you can use the pathway in presentations or papers.

![alt text](img/export.png "export")
