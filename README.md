# Day 2: Pathway and network analysis @NuGO Postgraduate course: Multiplatform-omics data analysis in nutrigenomics studies

* **Author:** Susan Coort

* **Last updated** 2020-09-08

----

### Introduction
In this tutorial you will get the opportunity to explore which insights modern-day large experimental screening methods can provide on the molecular effects of micronutrients. You will mainly study genome-wide gene expression (transcriptomics) and metabolomics data, using pathway and network analysis approaches. Before running such tools one has to preprocess (normalise) and statistically analyse the data at hand, in order to create estimates of the difference in gene expression before and after exposure to the nutrient being studied. You will receive datasets for which these steps have already been applied. You will conduct pathway analysis using the computer programme PathVisio [1] with the curated pathway collection of the open repository WikiPathways [2,3]. PathVisio applies overrepresentation analysis to detect pathways in which more differentially expressed genes (genes of which expression changes after the nutrient is provided) are present than expected by chance. For Network analysis you will use the open-source tool Cytoscape [4].  Both tools provide visualisation options to get a better insight in the analytical results and help to get biological understanding of the gene expression changes at the cellular level after treatment with the hormonally active form of vitamin D.  

----

### Goals
* Learn how to perform pathway analysis using the computer programme, PathVisio, with the curated pathway collection of the open repository WikiPathways.
* Learn how to visualize multi-omics data on biological processes.
* Learn how to visualize pathways in the network analysis program, Cytoscape. 
* Learn how to extend a biological network with additional information. 

----

### Organization
* Dr. Susan Steinbusch-Coort, Assistant Professor at the Department of Bioinformatics (BiGCaT), NUTRIM School for Nutrition and Translational Research in Metabolism, Maastricht University. 
** Contact: susan.coort@maastrichtuniversity.nl 
* Dr. Lauren Dupuis, Postdoc at the Department of Bioinformatics (BiGCaT), NUTRIM School for Nutrition and Translational Research in Metabolism, Maastricht University. 

----

### Schedule and setup
Wednesday September 9th, 2020

| Time | Topic | Type |
| ---- | ----- | ---- |
| 09:00-09:45 | Introduction on biological pathways databases and pathway analysis | Lecture |
| 09:45-11:00 | Hands-on pathway analysis | Tutorial |
| 11:00-11:10 | Coffee break |    |
| 11:10-11:40 | Introduction in network analysis for multi-omics data | Lecture |
| 11:40-12:30 | Network analysis in Cytoscape using several apps | Tutorial |
| 12:30-12:45 | Q & A session |   |
| 12:45-13:30 | Lunch break |     | 
----

#### Lecture: Pathway analysis using various omics data
* [Handouts Pathway analysis lecture](https://surfdrive.surf.nl/files/index.php/s/tzdkRXNyReByJvG)
* During lecture an introduction to pathway analysis will be given. First, all the ingredients for pathway analysis, i) biological pathways (available at [WikiPathways](https://www.wikipathways.org/) [2,3]), ii) identifier mapping and iii) omics data, will be explained. Second, the in-house developed and open-source pathway visualization and analysis tool, [PathVisio](https://pathvisio.github.io/) [1] will be introduced. Finally, examples of pathway visualization using various omics data, like transcriptomics and proteomics will be demonstrated.


#### Lecture: Pathway analysis using various omics data
* [Handouts Network analysis lecture](https://surfdrive.surf.nl/files/index.php/s/dYsh1UnacmP2BHR)
* During lecture an introduction to network nalysis will be given. First it will be explained what bioligical networks are. Which resousrces are available for biological networks. An overview of these resources is available at [PathGuide](http://www.pathguide.org/). Moreover, how tp extend biological networks with compound and pathway information will be demonstrated using the network analysis tool [Cytoscape](https://cytoscape.org/)[4]
----

### Literature
1. Kutmon M, et al. "PathVisio 3: an extendable pathway analysis toolbox." PLoS Comput Biol 11.2 (2015): e1004085. [doi: 10.1371/journal.pcbi.1004085](https://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1004085).
2. Kutmon M, et al. "WikiPathways: capturing the full diversity of pathway knowledge." Nucleic acids research (2015): gkv1024. [doi: 10.1093/nar/gkv1024](https://academic.oup.com/nar/article/44/D1/D488/2502580).
3. Slenter DN, et al. "WikiPathways: a multifaceted pathway database bridging metabolomics to other omics research." Nucleic Acids Res (2018)  Jan 4;46(D1):D661-D667. [doi: 10.1093/nar/gkx1064](https://academic.oup.com/nar/article/46/D1/D661/4612963).
4. Smoot ME, et al. "Cytoscape 2.8: new features for data integration and network visualization." Bioinformatics. (2011) Feb 1;27(3):431-2. [doi: 10.1093/bioinformatics/btq675](https://academic.oup.com/bioinformatics/article/27/3/431/321742)
